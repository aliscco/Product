## Product Manager Responsibility 

### Weekly

[][][][] Walk the board with your EM(s)
[][][][] Review `deliverable`, `direction`, `planning-priority`, and `feature` issue status
[][][][] Meet and/or talk to a customer
[][][][] Review priority ranking of issues in current milestone
[][][][] Review priority ranking of issues in `planning breakdown` 
[][][][] Review priority ranking of issues in `scheduling` 
[][][][] Review stage capacity planning

### Monthly

[] Review and update cateogry direction page(s)
[] Review group area direction page(s)
[] Review and update category.yml 
[] Review and update feature.yml 

#### Monthly Release Post Tasks
by 4th of the Month:

[] Draft of the issues that will be included in the next release post
[] Start capacity and technical discussions with engineering and UX

by 12th of the Month:

[] Release scope is finalized. In-scope issues assigned a milestone
[] Ensure `direction` and/or `planning priority` have been labeled as `deliverable`
[] Kickoff document is updated with relevant items to be included.

by 17th of the Month:

[] Group Kickoffs calls recorded and uploaded.
