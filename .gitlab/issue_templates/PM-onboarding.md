## Common tasks for ramping up as a PM

For some of the links below, you'll need to know your group label. Typically, this will just be in the format `group::monitor`. If you are unsure of your group, please check with your manager. The links as written will take you to the Monitor group board as an example, simply modify the label query with your own group name to see your own board. You may also subscribe to any label, which will notify you via email each time an issue with that label is updated. 

It will also be very helpful to know who your teammates are for your section, stage, and group. This information is on the [categories handbook page](https://about.gitlab.com/handbook/product/categories/#devops-stages).

### Week 1
* [ ] Focus on general GitLab onboarding

### Week 2 
<!-- TODO: Change the issue id to the new PM's issue id in the first link below -->
* [ ] Complete Product Section of your general onboarding ticket
* [ ] Create a new issue in the [Product project](https://gitlab.com/gitlab-com/Product/issues/) titled "Learning the PM Process at GitLab". Treat this issue as a discovery issue and use it as a notepad about what you're learning and where you're facing challenges.
* [ ] Read and familiarize yourself with the [main Product Handbook pages](https://about.gitlab.com/handbook/product/)
  * [ ] [Product Manager Responsibilities](https://about.gitlab.com/handbook/product/product-manager-responsibilities/)
  * [ ] [Product Processes](https://about.gitlab.com/handbook/product/product-processes)
  * [ ] [Product Principles](https://about.gitlab.com/handbook/product/product-principles/)
  * [ ] [Being a product manager at GitLab](https://about.gitlab.com/handbook/product/product-manager-role/)
  * [ ] Familiarize yourself with the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). It's important to know where we are in the cadence and start to work ahead as quickly as possible.
  * [ ] [product development flow](https://about.gitlab.com/handbook/product-development-flow/index.html). Pay attention to how labels, such as `workflow::xxxxx`, `direction`, `deliverable`, and `planning priority` work.
* [ ] Coordinate with your PM onboarding buddy and shadow their team and customer meetings for the week.
* [ ] Schedule a call with a different PM to talk about their biggest "lessons learned" from being a PM @ GitLab
* [ ] Ask questions in the #product Slack channel to get clarification on things you are uncertain about
* [ ] Review the boards that your development group uses for planning, scheduling, building, or other activities. If you don't know where they are, ask your EM!
* [ ] Attend your group's weekly meetings and introduce yourself to the team.
* [ ] Explore the [Product Management Learning and Development handbook page](https://about.gitlab.com/handbook/product/product-manager-role/learning-and-development/) and corresponding [Product Team Learning Hub in GitLab Learn](https://gitlab.edcast.com/channel/gitlab-product-team-learning-hub). All content in GitLab learn must be in the handbook but some handbook content isn't yet in GitLab learn, so check out both! 
  * [ ] Complete the [GitLab Learn training on Iteration](https://gitlab.edcast.com/channel/gitlab-product-team-learning-hub)
  * [ ] Assign interesting topics/content to yourself in the Learning Hub.
* [ ] Familiarize yourself with [Triage issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&search=%22triage+report%22), which are created for groups automatically. Update [team-triage-package.yml](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/stages/package/team-triage-package.yml) to assign these issues to yourself for your group.

### Week 3
* [ ] Review [recent release posts](https://about.gitlab.com/blog/categories/releases/), in particular the main monthly releases, for what's been changing in your stage.
* [ ] Review recent analyst research in the #analyst-relations slack channels relevant to your stage and category.
  * [ ] Setup a meeting with Analyst Relations Manager Ryan Ragozzine (`@rragozzine`) to introduce yourself, receive relevant research, and begin getting involved in analyst activities in your area. 
* [ ] Coordinate with your engineering manager on what will be delivered in the current release; help ensure [release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/) content is created merged at the appropriate time in time. If you need help, ask a fellow PM, your manager, or your EM.
* [ ] Using your "Learning the PM Process at GitLab" issue and one of the challenges you encountered, create an MR to add that solution to the handbook.
* [ ] Review [the direction page](https://about.gitlab.com/direction), in particular the sub-page for your stage and the direction pages for the categories you manage. You can find links directly to your category vision pages on the categories handbook page. You will be responsible for updating and maintaining your direction page(s) with what your group is working on and why. 
* [ ] Revisit the Product Handbook and product development pages now that you've had some experience under your belt. Prepare any questions you have on it for discussion with your manager in your next 1x1.
* [ ] Ping @mallen001 to open an issue for interview training
* [ ] Read about the [UX Research shadowing process](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-shadowing). You can kick off the process when you meet with the UX Researcher for your group. Product Managers should go through research shadowing before executing UX research on their own.

### Week 4
* [ ] Organize your boards, issues, and epics. It is likely things have gotten unorganized over time
  * [ ] Organize your group issues into [Epics](https://about.gitlab.com/handbook/product/#meta-epics-for-longer-term-items) including [Category Maturity Plan Epics](https://about.gitlab.com/handbook/product/#maturity-plans). 
  * [ ] Don't hesitate to [close issues as needed](https://about.gitlab.com/handbook/product/#when-to-close-an-issue). 
* [ ] Figure out who your internal customers are. Set up regular check-ins in with your Internal Customers (also found on the categories handbook page); try to aim for a monthly cadence. 
* [ ] Reach out to users via popular issues and schedule user interviews to better understand the problems our users face.
* [ ] Complete interview training
/label ~onboarding ~Doing
