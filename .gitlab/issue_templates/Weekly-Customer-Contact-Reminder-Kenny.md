## Intent
Ensure I'm contacting 3 customers a week on my way to 100 customers a year.

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Consider whether you need another source
* [ ] If first week of the month - Consider increasing the number of schedule outreach to ensure you are hitting goal
* [ ] If first week of the quarter - Consider additional sources of feedback

### :pencil2: Review List
* [ ] Review [Q1 NPS Feedback](https://docs.google.com/spreadsheets/d/1Lw_bbTz4K15H8ClFMGO6tiQXrKgCL3Lf_yDAiNOXLy4/edit#gid=0) - [Q4](https://docs.google.com/spreadsheets/d/1mwps_sDbBeu9DnH-MCs2okvyYlli6HKb3mniMuVXwyU/edit#gid=0)/([Q1](https://docs.google.com/spreadsheets/d/1JXy4p8C9f9qvXLVisdSMoF_zKLfQhP6SKG3S1M5E9zk/edit#gid=0)

### :calendar: Schedule
Using this template and [Markdown Here](https://chrome.google.com/webstore/detail/markdown-here/elifhakcjgalahccnjkneoccemfahfoa?hl=en)
```
👋  Hi!

You recently completed a GitLab NPS survey and left open the option of someone from GitLab contacting you as a follow-up. Well guess what?! I'm [that person](https://gitlab.com/kencjohnston). I'd love to set up a time to chat. If you are up for it you can respond to this email or use my [Calendly link](https://calendly.com/gitlab-kjohnston) to book a time that works for you. 

Thanks!
```
* [ ] Send :six: email requests with Calendly invite


### :thinking: Reflect
* [ ] Review [notes from previous conversations](https://docs.google.com/document/d/1HrkMtl27EJV4ofimVwWtwVfRErShenv8TTHSljkf-8c/edit)
* [ ] Update status in NPS Survey Respondent sheet
* [ ] Compile docs and issue links and respond to customers via email
* [ ] Think about conversations you've had with customers any broad themes?

### :diamond_shape_with_a_dot_inside: Organize
* [ ] Organize Responses in Dovetail ([example](https://dovetailapp.com/projects/65f3323b-3b7c-41c6-9efb-a8066c9369ba/readme))

### :mega: Communicate
* [ ] Broadcast MRs or lessons learned in #product-standup and appropriate slack channels

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Customer-Contact-Reminder-Kenny.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops" 

/confidential

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338
