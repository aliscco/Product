## :dart: Intent
The intent of this issue is to review and prioritize any Ops Section Investment Cases.

## :book: References
- [Current +10 Investment Opportunities]() 
- [Proposed Investment Case Board](https://gitlab.com/gitlab-com/Product/-/boards/2427050?&label_name%5B%5D=Investment%20Case&label_name%5B%5D=section%3A%3Aops) 

## Tasks

### :wave: Open
* [ ] Create retrospective Thread
* [ ] Ask PM Leaders if there are any pending Investment Cases to document
* [ ] Set Due Date for when review will begin

### :pencil2: Review Actions
Actions for reviewing and prioritizing
* [ ] Review and Prioritize Investment Cases on the board - @kencjohnston
* [ ] Update the +10 Handbook Content - @kencjohnston
* [ ] Note any updates in Slack - @kencjohnston


### :x: Close
* [ ] Record a video summary of our current and historical investment. Share in Slack and add to the Direction page.
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Monthly-Investment-Case-Review.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops"
