### Overview 

This issue will serve as a tracker for the transition from `Current role` with `Current stage/group` to `New role` with `New stage/group`. 

### Transition Overview 

* Effective Date: YYYY-MM-DD
* Announcement Date: YYYY-MM-DD
* `Current role` Duties: 
   * Assignee: Tasks
   * Assignee: Tasks
   * Assignee: Tasks

### Opening Tasks 

* [ ] Create transition issue 
* [ ] Share issue with Manger 
* [ ] Once announcement is final move to Product Project 
* [ ] Add retrospective thread for updates to template
* [ ] Share relevant documents with parties 

### Timeline 

#### Week 1

* [ ] Update direction pages 
* [ ] Share issue in your impacted channels 
* [ ] Collect documents and add to `Relevant Documents` section 
* [ ] Add Task
* [ ] Add Task

#### Week 2

* [ ] Schedule syncs with Transition Parties (if needed) 
* [ ] Transition any documents, open problem/solution validation 
* [ ] Transition open release post MRs for after 13.6
* [ ] Transition any customer calls scheduled to new PM 
* [ ] Share planning issues
* [ ] Update team.yml and categories.yml 
* [ ] Bulk update issues 
* [ ] Update reporting structure in Bamboo - @kencjohnston 
  
### Relevant Documents 

* [Add Document](URL)
* [Add Document](URL)
* [Add Document](URL)

### Closing Tasks 

* [ ] Mark all tasks as complete 
* [ ] Update template with retrospective thread items
* [ ] Close Issue
