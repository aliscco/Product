## :book: References
- [SalesForce Current Quarter Opportunity Dashboard](https://gitlab.my.salesforce.com/01Z4M0000007H7W)
  - [Kenny's Custom Commit Report](https://gitlab.my.salesforce.com/00O4M000004aZ98)
  - [Kenny's Custom Best-Case Report](https://gitlab.my.salesforce.com/00O4M000004aj4Q)
  - [Commit Report](https://gitlab.my.salesforce.com/00O61000004hfX2?dbw=1) - NOTE: Adjust "Show" to "All Opportunities"
  - [Best-Case Report](https://gitlab.my.salesforce.com/00O61000004hfWn?dbw=1) - NOTE: Adjust "Show" to "All Opportunities"
  - [Tech Eval Opportunities](https://gitlab.my.salesforce.com/00O4M000004acjz)
  - [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=opportunity+review&scope=all&sort=created_date&state=closed&utf8=%E2%9C%93)

## :dart: Intent
**To develop a better understanding of Top Opportunities in our Sales Pipeline within the product team.** This issue is a weekly template that Product team members participate in to better understand late-stage (`Commit`) opportunities as well as recently (`Tech Eval`) opportunities. We primarily review them from an angle for what the product team can learn about the types of customers we successfully attract and satisfy and specifics about those individual customer opportunities that helped us get into the late-stage position or pass/fail a tech eval. 

## :rocket: Opportunities to Review
<!--- * Opportunity Name - `@assigned individual` --->

### Commit Opportunities
Evaluate four current quarter commit opportunities per week
- [ ] []() - @
- [ ] []() - @
- [ ] []() - @
- [ ] []() - @

### Recent Tech Eval Opportunities
Evaluate two recent tech eval opportunities per week.
- [ ] []() - @
- [ ] []() - @

### Best-Case Opportunities
Evaluate one upcoming best-case opportunity per week.
- [ ] []() - @kencjohnston

## :white_check_mark: Tasks

### :airplane_departure: Monthly/Quarterly Tasks
- [ ] If the first week of the Quarter - look back across previous reviews for themes - @kencjohnston
- [ ] If the first week of the Quarter - solicit new PMs to participate - @kencjohnston
- [ ] if the first week of the Quarter - consider other opportunity types for investigation (losses)

### :o: Opening Tasks
- [ ] Create retrospective thread - @kencjohnston
- [ ] Determine un-investigated opportunities from [commit](https://gitlab.my.salesforce.com/00O4M000004aZ98), [Tech Eval](https://gitlab.my.salesforce.com/00O4M000004acjz) and [best-case](https://gitlab.my.salesforce.com/00O4M000004aj4Q) opportunities and assign based on counts in the issue template - @kencjohnston
- [ ] Assign to team members for individual updates - current pool is (Orit, Kevin, Gabe, Kenny, Viktor)

### :crown: Assigned Opportunity Tasks
- Understand by checking the SFDC opportunity (specifically the [command plan](https://about.gitlab.com/handbook/sales/command-of-the-message/command-plan/)), [account project](https://drive.google.com/drive/u/0/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U), slack channel and meeting notes
- Add a comment with the following format:

#### For Commit Opportunities
```
## Commit Opportunity - [Account Name]

[Account Name](Opportunity Link) - REPLACE ME - brief description of the business (what do they do, how are they organized, what will they use GitLab for) and opportunity (new business, upgrade, expansion, tech eval result)

Highlights from [Command Plan](CP Link) (PM's mentioned as FYI):

1. Highlight - PM

Insights for the team. If warranted tasks for additional updates in slack or handbook updates.

:wave: Hi [ACCOUNT TEAM] - you can check out the [intent](#dart-intent) of this issue, here are some questions I have about this opportunity:

1. Question
```

#### For Tech Eval Opportunities
```
## Tech Eval Opportunity - [Account Name]

[Account Name](Opportunity Link) - REPLACE ME - brief description of the business (what do they do, how are they organized, what will they use GitLab for) and opportunity (new business, upgrade, expansion, tech eval result)

Highlights from [Command Plan](CP Link) (PM's mentioned as FYI):

1. Highlight - PM

:wave: Hi [ACCOUNT TEAM] - you can check out the [intent](#dart-intent) of this issue, here are some questions I have about this opportunity:

- Why we won?
- Any hurdles that had to be overcome in the eval?
- Any required capabilities that were part of the eval that we didn't match?
- Any capabilities that we show-cased that really won over the customer?
```

- Post insights in appropriate channels

#### :up: Individual Updates
Unassign yourself when your above tasks are complete.

### :x: Closing Tasks
* [ ] Add a summary/highlights comment and share it in Slack - @kencjohnston
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Top-Opportunity-Review.md) based on the retrospective thread - @kencjohnston

/confidential

/label ~"opportunity review" 

/assign @kencjohnston

/due Thursday
