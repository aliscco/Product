## Intent
To increase awareness of our SaaS Performance indicators for product groups with particularly high usage across the section and company. To provide a regular, timely review of those performance indicators to spot trends and mobilize response quickly.

https://about.gitlab.com/handbook/product/categories/ops/#saas-reviews

## References
- [PE Error Budget](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#verifypipeline-execution---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-pipeline_execution/stage-groups-group-dashboard-verify-pipeline-execution?orgId=1&from=now-28d&to=now)
- [Runner Error Budget](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#verify-verifyrunner---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-runner/stage-groups-group-dashboard-verify-runner)
- [Runner SLO Attainment](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#verifyrunner---other---ci-runners-slo-trends) - [Grafana Dashboard](https://dashboards.gitlab.com/d/general-slas/general-slas?from=1609459200000&orgId=1&to=now&viewPanel=23)
- [Package Error Budget](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#packagepackage---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-group-dashboard-package-package)
- [Testing Error Budget](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#verify-verifytesting---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-testing/stage-groups-group-dashboard-verify-testing?orgId=1)
## Review Format
Provide updates in the [ops_section.yaml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/performance_indicators/ops_section.yml) for your preformance indicators inline with our standard performance indicator review (update health and add `Insights` and `Improvements`).

## Tasks

### Opening Tasks
- [ ] Retro Thread - @kencjohnston

### Review Tasks
- [ ] Verify:PE Review Completed and Shared in Slack - @jreporter
- [ ] Verify:Runner Review Complete and Shared in Slack- @darreneastman
- [ ] Package:Package Review Complete and Shared in Slack - @trizzi
- [ ] Verify:Testing Review Complete and Shared in Slack - @jheimbuck_gl

### Closing Tasks
- [ ] Post Highlights
- [ ] Review in Ops Section Manager Meeting
- [ ] [Updates](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Ops-Async-SaaS-Review.md)

/assign @jreporter @darreneastman @trizzi @sgoldstein @kencjohnston @dcroft @jheimbuck_gl

/due Monday
