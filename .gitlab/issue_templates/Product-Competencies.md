<!-- Every GitLab team member is encouraged to contribute to the list of prioritized [Product Manager competencies](https://about.gitlab.com/handbook/product/product-manager-role/#contributing-to-our-pm-competencies). This issue template is the guide for contributors. -->

### Opening tasks
* [ ] Assign to manager and VP of Product `@adawar` for approval and merge 

### How to contribute to the Product Competencies
* [ ] Open an MR to [Product Competencies Page](https://about.gitlab.com/handbook/product/product-manager-role/#future-competencies)
* [ ] Select a competency from the [Future Competencies section](https://about.gitlab.com/handbook/product/product-manager-role/#future-competencies)
* [ ] Copy selected competency and move to appropriate section, "Validation Track", "Build Track", or create a new section if one does not exist 
* [ ] Remove selected competency from Furture Competencies List 
* [ ] Copy/paste the table format in the [Customer Interviewing Competency](https://about.gitlab.com/handbook/product/product-manager-role/#customer-interviewing-competency) under the selected competency and remove the Customer Interviewing content
* [ ] Update table with necessary skills by role Tip: Consider tiering difficulty, expertise, or extensiveness of support of each task or skill as the role grows 
* [ ] Create a Google Form Assessment for competency of individual contributor as well as manager. Make sure to give access to everyone to take. Add `product leadership` as a contributor to the Google Form 

### Share competency update
* [ ] Post merge request in: 
     * [ ] #product

### Closing tasks 
* [ ] Add retrospective thread to make improvements to this tenplate 
* [ ] Close this issue

/label ~product 
/assign `@PM` 
