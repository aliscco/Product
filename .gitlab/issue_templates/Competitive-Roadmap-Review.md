## Monthly Competitive Roadmap Review

1. Review competitor roadmaps
1. Share insights this issue
1. Follow up with individual groups/categories

## References
* GitHub: https://github.com/github/roadmap/projects/1
  * Changelog is available by clicking on `Menu` on the right-hand side
* Atlassian/BitBucket: https://www.atlassian.com/roadmap/cloud?selectedProduct=bitbucket
* [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Competitive%20Roadmap%20Review)

## Opening
* [ ] Create Retrospective Thread - @joshlambert
* [ ] Set the issue due date for mid-month - @joshlambert

## Tasks 

### Section reviewers
Review for implications to your sections groups and direction. Provide a highlight comment and ping your team

* [ ] Dev: @david
* [ ] Ops: @kencjohnston 
* [ ] Secure/Defend: @hbenson
* [ ] Enablement: @joshlambert 

### Group reviewers

* [ ] Global Search: @JohnMcGuire

## Closing
* [ ] Make [updates to this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Competitive-Roadmap-Review.md) based on retrospective thread - @joshlambert

/assign @david @kencjohnston @joshlambert @JohnMcGuire

/label ~"Competitive Roadmap Review"
