### Request for recruting customers via In App Messaging

Ensuring a positive user experience for our users is the most important factor to consider when deploying messaging in our product. With that goal in mind, we have put in place the following procedures to ensure that in-app messaging does not result in any negative user sentiment.

#### What is the purpose of your proposed messaging?
<!-- Please fill out -->

#### How will you define success?
<!-- Please fill out -->

#### What type of users do you want to target? (be as specific as you can) 
<!-- Please fill out -->

#### How many impressions you are targeting for your desired user type(s)?
<!-- Please fill out -->

#### What pages do you plan to deploy your message on?
<!-- Please fill out -->

#### What is the average total of unique users accessing those pages per day?
<!-- Please fill out -->

#### What is the average unique users matching your desired user type(s) accessing those pages per day?
<!-- Please fill out -->

#### What type of message is needed:
<!-- Please fill out -->

- Informational (no CTA)
- Link (copy & CTA)
- Survey (copy & in-line question)

#### What is the desired content of your message?

If your message contains a survey (either in-line or linked), you’ll need to fill out a problem validation research request issue in the UX Research project where a UX Researcher will review and approve your survey. Include a link to your research issue.

The VP of Product Management will review and prioritize as needed, and then assign to Senior Growth Product Manager (for deployment/monitoring) and Senior UX Researcher (for reviewing messaging content/number of impression, etc.)

#### General in-app messaging guidelines

- Messages must be approved by VP of Product Management @adawar in order to run.
- We run a maximum of 2 in-app messages at any given time in order to limit the number of messages a user sees in quick succession.
- Messages run for the shortest amount of time possible.
- Messages are limited to a single page on the site unless there is a business reason to put it on multiple pages.

#### Transparency around decision making criteria

The goal will be weighed against in-app message fatigue. Will we have made significant progress on outcomes or will we have frustrated our users?

/label ~In-App-Messaging ~"Product Operations"
/assign @adawar @kokeefe