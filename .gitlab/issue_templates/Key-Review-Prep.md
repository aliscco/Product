**Next Product Key Review:**

**Meeting Agenda:**
https://docs.google.com/document/d/1Hmg2r-VGYUtYqlHDoPCzeJPI350Y58JSGVHy-P33UBc/edit

**Link to Google slides for this month's Key Revew - Product:**

**Link to Google Folder with all Decks:**
https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg

### Tasks: 

* Review the KPI slides for metrics you own and add any necessary commentary either to the graph slides or the key takeaways slide 
  * [ ]  @mkarampalas 
  * [ ]  @jstava 
  * [ ]  @s_awezec
* Update [Highlights Release Selector](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/sites/handbook/source/includes/product/_highlights.erb) - @kencjohnston
* Update your Section Highlights Slide
  * [ ]  @david
  * [ ]  @kencjohnston
  * [ ]  @joshlambert
  * [ ]  @justinfarris
  * [ ]  @hilaqu
  * [ ]  @keithchung
* [ ] Update the FY21 Q3 okr slides that you own - @justinfarris
* [ ] Update the FY21 Q3 okr slides that you own - @hilaqu
* [ ] Update the NPS related slides -  @fseifoddini 
* [ ] Update the Category Maturity KPI related slides -  @fseifoddini
* [ ] Updates the slides you own - @adawar
* Review relevant Data Team Key Meeting Slides
  * [ ]  @justinfarris
  * [ ]  @mpeychet_
  * [ ]  @dpeterson1 
* [ ] Review full deck before Key Meeting - @sfwgitlab 
* [ ] Add a "Retrospective Thread" comment to this issue - Anyone
* [ ] After each Key Meeting [update this Key Review prep issue template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Key-Review-Prep.md) with any added items - All
    - For next month, make a copy of this month's Key Review Slides and save them in the [Key Review - Product Folder](https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg)

Thank you all for your help!


/assign @kristie.thomas
