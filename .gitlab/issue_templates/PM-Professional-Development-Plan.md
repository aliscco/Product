<!-- Title this issue: Name of PM - Development Plan for CDF XXXX-XX-XX  -->
<!-- Use this issue to create and document a growth plan for a PM to progress to the next compa ratio. This template is based on the [four main tracks](https://about.gitlab.com/handbook/product/product-manager-role/#competencies)  -->

This issue details a professional development plan for a Product Manager.
## Overview
<!-- This section provides details on the PM in question. Please fill out the details  -->
**Personal Details:**  
- Product Manager: `@PM`
- Manager: `@PM Manager`
- Role/Title: <!--Intermediate, Senior, or Principal -->
- [Link to CDF]()

**Helpful References:**  
- [Product Management tracks & competencies](https://about.gitlab.com/handbook/product/product-manager-role/#competencies)
- [CDF Review Process](https://about.gitlab.com/handbook/product/product-manager-role/#cdf-reviews)

## Goals
<!-- Document high level goals. Make sure they are quantifiable. Example: Move from Growing to Thriving compa ratio, submit for compensation review adjustment, get promoted to Principal, etc -->
1. 
2. 
3. 


### Current CDF assessment
<!-- Fill out the table with current assessment values (Learning, Growing, Thriving, Expert) for each track  -->
Here is a summary of the current assessment of the fundamental tracks:

| Discovery Track| Build Track | Business Skills | Communication Skills |
| ------ | ------ | ------ | ------ | 
|  |  | |  |  |

### Areas that require focus
<!-- List tracks/competencies requiring focus. Not all competencies need focus at the same time. Identify those the need the most investment. -->
1. 
2. 


## Plan
<!-- Make the plan detailed. Each track comprises multiple competencies. List the competency in the first column, followed by   -->

| Track |Competency to develop|Quantifiable Action|Documentation|Timeline|Maps to GitLab Value|
|-------|---------------------|-------------------|-------------|--------|--------------------|
|       |                     |                   |             |        |                    |


## Tasks 
- [ ] Assign issue to yourself
- [ ] Set due date as date of next CDF review
- [ ] Create a Retrospective Thread
- [ ] Fill out personal details in the Overview section
- [ ] Fill out summary of CDF assessment
- [ ] List areas of growth resulting from most recent CDF
- [ ] Draft plan
- [ ] Get product leadership review and sign-off on plan
   - [ ] `@ your manager` 
   - [ ] `@ your section leader`
- [ ] Complete and document all items in plan. Confirm CDF adjustment with `@PM Manager`
- [ ] Contribute suggested improvements from the Retro thread to [this issue template](https://gitlab.com/gitlab-com/Product/-/edit/master/.gitlab/issue_templates/PM-Professional-Development-Plan.md)